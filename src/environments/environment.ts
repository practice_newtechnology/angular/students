// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyB4x_n_oA6iHKtk9ettOne34sfTYx3q2QE",
    authDomain: "students-7ca9e.firebaseapp.com",
    projectId: "students-7ca9e",
    storageBucket: "students-7ca9e.appspot.com",
    messagingSenderId: "1034086914283",
    appId: "1:1034086914283:web:105be8115a1a21b5b146ae",
    measurementId: "G-X84VYZH82L"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
