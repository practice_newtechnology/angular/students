import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  companyLoginForm: FormGroup;
  studentLoginForm: FormGroup;
  loginType = true;
  companyList;

  constructor(
    private fb: FormBuilder,
    private angularFirestore: AngularFirestore,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.angularFirestore.doc(`generalModule/company`).valueChanges().subscribe((data: any) => {
      this.companyList = data.companyList;
    });
    this.createLoginForm();
  }

  createLoginForm() {
    this.companyLoginForm = this.fb.group({
      username: ['', [ Validators.required ]],
      password: ['', [ Validators.required ]],
      company: ['', [ Validators.required ]]
    });

    this.studentLoginForm = this.fb.group({
      email: ['', [ Validators.compose([Validators.required, Validators.email]) ]],
      password: ['', [ Validators.required ]],
      company: ['', [ Validators.required ]]
    });
  }

  get username() { return this.companyLoginForm.get('username') as FormControl }
  get passwordCP() { return this.companyLoginForm.get('password') as FormControl }
  get companyCP() { return this.companyLoginForm.get('company') as FormControl }
  get email() { return this.studentLoginForm.get('email') as FormControl }
  get passwordST() { return this.studentLoginForm.get('password') as FormControl }
  get companyST() { return this.studentLoginForm.get('company') as FormControl }

  companyLogin() {
    if(this.loginType) {
      this.angularFirestore.collection(`users`, ref => {
        let query : any = ref;
        if (this.companyLoginForm.value.username) { query = query.where('username', '==', this.companyLoginForm.value.username) };
        if (this.companyLoginForm.value.password) { query = query.where('password', '==', this.companyLoginForm.value.password) };
        if (this.companyLoginForm.value.company) { query = query.where('company', '==', this.companyLoginForm.value.company) };
        return query;
      }).valueChanges().subscribe(data => {
        if(data?.length) {
          this.router.navigate([`company/${this.companyLoginForm.value.company}`, { outlets: { right: 'studentlist'}}]);
        }
      })
    }
  }

  studentLogin() {
    this.angularFirestore.collection(`/generalModule/students/${this.studentLoginForm.value.company}`, ref => {
      let query : any = ref;
      if (this.studentLoginForm.value.email) { query = query.where('email', '==', this.studentLoginForm.value.email) };
      if (this.studentLoginForm.value.password) { query = query.where('password', '==', this.studentLoginForm.value.password) };
      return query;
    }).valueChanges().subscribe((student: any) => {
      if(student?.length) {
        this.router.navigate([`student/${this.studentLoginForm.value.company}/${student[0].id}`, { outlets: { right: 'courselist'}}]);
      }
    })
  }

}
