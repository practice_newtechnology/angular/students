import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { ActivatedRoute, Router } from '@angular/router';
import { combineLatest, Subject } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-my-cart',
  templateUrl: './my-cart.component.html',
  styleUrls: ['./my-cart.component.scss']
})
export class MyCartComponent implements OnInit {
  companyId;
  companyId$ = new Subject<any>();
  myIds$ = new Subject<any>();
  studentId;
  myCartList;
  ratings = [
    { isSelected: true },
    { isSelected: true },
    { isSelected: true },
    { isSelected: false },
    { isSelected: false }
  ];

  getAllCourse$ = this.companyId$.pipe(
    switchMap(companyId => {
      return this.angularFirestore.collection(`generalModule/courses/${companyId}`).valueChanges();
    })
  );

  getMyCart$ = this.myIds$.pipe(
    switchMap((id: any) => {
      return this.angularFirestore.collection(`generalModule/mycart/${id[0]}/${id[1]}/cartlist`).valueChanges();
    })
  );

  getMyCartList$ = combineLatest([this.getAllCourse$, this.getMyCart$]).pipe(
    map((data: any) => {
      data[0] = data[0].filter(x => data[1].some(y => y.courseId == x.id));
      return data[0];
    })
  ).subscribe(courselist => {
    this.myCartList = courselist;
  });

  constructor(
    private angularFirestore: AngularFirestore,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.companyId = this.route.snapshot.parent.params['companyId'];
    this.companyId$.next(this.companyId);
    this.studentId = this.route.snapshot.parent.params['studentId'];
    this.myIds$.next([this.companyId, this.studentId]);
  }

  buyAllCourses() {
    this.myCartList.forEach((course: any) => {
      this.angularFirestore
      .collection(`generalModule/mycart/${this.companyId}/${this.studentId}/cartlist`,  ref => {
        let query : any = ref;
        if (course.id) { query = query.where('courseId', '==', course.id) };
        return query;
      }).valueChanges().subscribe(data => {
        if(data?.length) {
          data.forEach((cart: any) => {
            this.angularFirestore
            .doc(`generalModule/mycart/${this.companyId}/${this.studentId}/cartlist/${cart.id}`)
            .delete();
          })
        }
      });  
  
      this.angularFirestore
      .collection(`generalModule/mycourses/${this.companyId}/${this.studentId}/courselist`)
      .add({ courseId: course.id })
      .then((docRef: any) => {
        this.angularFirestore
        .doc(`generalModule/mycourses/${this.companyId}/${this.studentId}/courselist/${docRef.id}`)
        .update({ id: docRef.id });
      });  
    })
  }

  ngOnDestroy() {
    this.getMyCartList$.unsubscribe();
  }

}
