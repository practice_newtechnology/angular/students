import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { ActivatedRoute, Router } from '@angular/router';
import { BehaviorSubject, combineLatest, Subject } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-my-courses',
  templateUrl: './my-courses.component.html',
  styleUrls: ['./my-courses.component.scss']
})
export class MyCoursesComponent implements OnInit {
  companyId;
  companyId$ = new Subject<any>();
  myIds$ = new Subject<any>();
  searchCourses$ = new BehaviorSubject<string>('');
  studentId;
  myCourseList;
  ratings = [
    { isSelected: true },
    { isSelected: true },
    { isSelected: true },
    { isSelected: false },
    { isSelected: false }
  ];

  getAllCourse$ = this.companyId$.pipe(
    switchMap(companyId => {
      return this.angularFirestore.collection(`generalModule/courses/${companyId}`).valueChanges();
    })
  );

  getMyCourses$ = this.myIds$.pipe(
    switchMap((id: any) => {
      return this.angularFirestore.collection(`generalModule/mycourses/${id[0]}/${id[1]}/courselist`).valueChanges();
    })
  );

  getMyCoursesList$ = combineLatest([this.getAllCourse$, this.getMyCourses$, this.searchCourses$]).pipe(
    map((data: any) => {
      data[0] = data[0].filter(x => data[1].some(y => y.courseId == x.id));
      data[0] = data[0].filter(x => x.name.substring(0, data[2]?.length).trim().toLowerCase() == data[2]);
      return data[0];
    })
  ).subscribe(courselist => {
    this.myCourseList = courselist;
  });

  constructor(
    private angularFirestore: AngularFirestore,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.companyId = this.route.snapshot.parent.params['companyId'];
    this.companyId$.next(this.companyId);
    this.studentId = this.route.snapshot.parent.params['studentId'];
    this.myIds$.next([this.companyId, this.studentId]);
  }

  searchCourses(searchString) {
    this.searchCourses$.next(searchString.trim().toLowerCase());
  }

  ngOnDestroy() {
    this.getMyCoursesList$.unsubscribe();
  }

}
