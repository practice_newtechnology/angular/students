import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDrawer } from '@angular/material/sidenav';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-student-home',
  templateUrl: './student-home.component.html',
  styleUrls: ['./student-home.component.scss']
})
export class StudentHomeComponent implements OnInit {
  companyId;
  studentId;
  navList = [
    { heading: 'Course List', icon: 'home', isSelected: false, path: 'courselist' },
    { heading: 'My Courses', icon: 'view_list', isSelected: false, path: 'mycourses' },
    { heading: 'My Cart', icon: 'shopping_cart', isSelected: false, path: 'mycart' },
    { heading: 'Logout', icon: 'logout', isSelected: false, path: '' }
  ]
  currentNav;
  expandMenu = false;
  // @ViewChild('drawer') private drawer: MatDrawer;


  constructor(
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.companyId = this.route.snapshot.params['companyId'];
    this.studentId = this.route.snapshot.params['studentId'];
    let index = this.navList.findIndex(nav => nav.path == this.route.snapshot.children[0].routeConfig.path);
    this.navList[index].isSelected = true;
    this.currentNav = index;
  }

  changeNavigation(nav, index) {
    this.navList[this.currentNav].isSelected = false;
    nav.isSelected = true;
    this.currentNav = index;
    index == 3 ? this.router.navigate(['']) : this.router.navigate([`student/${this.companyId}/${this.studentId}`, { outlets: { right: nav.path}}]);
  }

}
