import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StudentRoutingModule } from './student-routing.module';
import { MaterialModule } from '../material.module';
import { StudentHomeComponent } from './student-home/student-home.component';
import { StudentCourseListComponent } from './student-course-list/student-course-list.component';
import { MyCoursesComponent } from './my-courses/my-courses.component';
import { MyCartComponent } from './my-cart/my-cart.component';


@NgModule({
  declarations: [
    StudentHomeComponent,
    StudentCourseListComponent,
    MyCoursesComponent,
    MyCartComponent
  ],
  imports: [
    CommonModule,
    StudentRoutingModule,
    MaterialModule
  ]
})
export class StudentModule { }
