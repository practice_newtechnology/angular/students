import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MyCartComponent } from './my-cart/my-cart.component';
import { MyCoursesComponent } from './my-courses/my-courses.component';
import { StudentCourseListComponent } from './student-course-list/student-course-list.component';
import { StudentHomeComponent } from './student-home/student-home.component';

const routes: Routes = [
  {
    path: ':companyId/:studentId',
    component: StudentHomeComponent,
    children: [
      {
        path: 'courselist',
        component: StudentCourseListComponent,
        outlet: 'right'
      },
      {
        path: 'mycourses',
        component: MyCoursesComponent,
        outlet: 'right'
      },
      {
        path: 'mycart',
        component: MyCartComponent,
        outlet: 'right'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StudentRoutingModule { }
