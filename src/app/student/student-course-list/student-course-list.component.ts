import { Component, OnDestroy, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { ActivatedRoute, Router } from '@angular/router';
import { BehaviorSubject, combineLatest, Subject, Subscription } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-student-course-list',
  templateUrl: './student-course-list.component.html',
  styleUrls: ['./student-course-list.component.scss']
})
export class StudentCourseListComponent implements OnInit, OnDestroy {
  companyId;
  companyId$ = new Subject<any>();
  myIds$ = new Subject<any>();
  searchCourses$ = new BehaviorSubject<string>('');
  studentId;
  otherCourseList;
  ratings = [
    { isSelected: true },
    { isSelected: true },
    { isSelected: true },
    { isSelected: false },
    { isSelected: false }
  ]

  getAllCourse$ = this.companyId$.pipe(
    switchMap(companyId => {
      return this.angularFirestore.collection(`generalModule/courses/${companyId}`).valueChanges();
    })
  );

  getMyCourses$ = this.myIds$.pipe(
    switchMap((id: any) => {
      return this.angularFirestore.collection(`generalModule/mycourses/${id[0]}/${id[1]}/courselist`).valueChanges();
    })
  );

  getMyCart$ = this.myIds$.pipe(
    switchMap((id: any) => {
      return this.angularFirestore.collection(`generalModule/mycart/${id[0]}/${id[1]}/cartlist`).valueChanges();
    })
  );


  getOtherCourses$ = combineLatest([this.getAllCourse$, this.getMyCourses$, this.getMyCart$, this.searchCourses$]).pipe(
    map((data: any) => {
      data[0] = data[0].filter(x => !(data[1].some(y => y.courseId == x.id)));
      data[0] = data[0].filter(x => x.name.substring(0, data[3]?.length).trim().toLowerCase() == data[3]);
      data[0].map(course => {
        course['isAddedToCart'] = (data[2].some(y => y.courseId == course.id));
        return course;
      })
      return data[0];
    })
  ).subscribe(courselist => {
    this.otherCourseList = courselist;
  });

  constructor(
    private angularFirestore: AngularFirestore,
    private router: Router,
    private route: ActivatedRoute
  ) { }
  

  ngOnInit(): void {
    this.companyId = this.route.snapshot.parent.params['companyId'];
    this.companyId$.next(this.companyId);
    this.studentId = this.route.snapshot.parent.params['studentId'];
    this.myIds$.next([this.companyId, this.studentId]);
  }

  searchCourses(searchString) {
    this.searchCourses$.next(searchString.trim().toLowerCase());
  }

  buyCourse(course) {
    this.angularFirestore
    .collection(`generalModule/mycart/${this.companyId}/${this.studentId}/cartlist`,  ref => {
      let query : any = ref;
      if (course.id) { query = query.where('courseId', '==', course.id) };
      return query;
    }).valueChanges().subscribe(data => {
      if(data?.length) {
        data.forEach((cart: any) => {
          this.angularFirestore
          .doc(`generalModule/mycart/${this.companyId}/${this.studentId}/cartlist/${cart.id}`)
          .delete();
        })
      }
    });  

    this.angularFirestore
    .collection(`generalModule/mycourses/${this.companyId}/${this.studentId}/courselist`)
    .add({ courseId: course.id })
    .then((docRef: any) => {
      this.angularFirestore
      .doc(`generalModule/mycourses/${this.companyId}/${this.studentId}/courselist/${docRef.id}`)
      .update({ id: docRef.id });
    });    
  }

  addToCartCourse(course) {
    if(course.isAddedToCart) {
      this.router.navigate([`student/${this.companyId}/${this.studentId}`, { outlets: { right: 'mycart'}}]);
    } else {
      this.angularFirestore
        .collection(`generalModule/mycart/${this.companyId}/${this.studentId}/cartlist`)
        .add({ courseId: course.id })
        .then((docRef: any) => {
          this.angularFirestore
          .doc(`generalModule/mycart/${this.companyId}/${this.studentId}/cartlist/${docRef.id}`)
          .update({ id: docRef.id });
      });
    }
  }

  ngOnDestroy() {
    this.getOtherCourses$.unsubscribe();
  }


}
