import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { ActivatedRoute, Router } from '@angular/router';
import { map } from 'rxjs/operators';
import * as XLSX from 'xlsx';

@Component({
  selector: 'app-company-home',
  templateUrl: './company-home.component.html',
  styleUrls: ['./company-home.component.scss']
})
export class CompanyHomeComponent implements OnInit {
  companyId;
  expandMenu = false;
  navList = [
    { heading: 'Student List', icon: 'people', isSelected: false, path: 'studentlist', match: 'studentlist' },
    { heading: 'Course List', icon: 'view_list', isSelected: false, path: 'courselist', match: 'courselist' },
    { heading: 'Create Student', icon: 'person_add', isSelected: false, path: 'student/null', match: 'student/:studentId' },
    { heading: 'Create Course', icon: 'playlist_add', isSelected: false, path: 'course/null', match: 'course/:courseId' },
    { heading: 'Logout', icon: 'logout', isSelected: false, path: '', match: '' },
  ]
  currentNav;

  constructor(
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.companyId = this.route.snapshot.params['companyId'];
    let index = this.navList.findIndex(nav => nav.match == this.route.snapshot.children[0].routeConfig.path);
    this.navList[index].isSelected = true;
    this.currentNav = index;
  }

  changeNavigation(nav, index) {
    this.navList[this.currentNav].isSelected = false;
    nav.isSelected = true;
    this.currentNav = index;
    index == 4 ? this.router.navigate(['']) : this.router.navigate([`company/${this.companyId}`, { outlets: { right: nav.path }}]);
  }

}
