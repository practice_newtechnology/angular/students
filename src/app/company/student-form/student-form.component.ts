import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import * as XLSX from 'xlsx';
@Component({
  selector: 'app-student-form',
  templateUrl: './student-form.component.html',
  styleUrls: ['./student-form.component.scss']
})
export class StudentFormComponent implements OnInit {
  studentForm: FormGroup;
  studentId;
  companyId;
  genders = [
    { value: 'male', view: 'Male'},
    { value: 'female', view: 'Female'}
  ];
  companyData;
  loading = true;
  studentDataExcel: any;


  departments = [
    { value: 'ECE', view: 'Electronics & Communication Engg' },
    { value: 'EIE', view: 'Electronics & Instrumentation Engg' },
    { value: 'CSE', view: 'Computer Science Engg' },
    { value: 'IT', view: 'Information Technology' }
  ]

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private angularFirestore: AngularFirestore,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.companyId = this.route.snapshot.parent.params['companyId'];
    this.studentId = this.route.snapshot.params['studentId'];
    this.angularFirestore.doc(`generalModule/company`).valueChanges().subscribe((data: any) => {
      this.companyData = data.companyList.find(company => company.id == this.companyId);
      this.createStudentForm();
      this.patchStudentDetails();
      this.loading = false;
    });
  }

  patchStudentDetails() {
    if(this.studentId != 'null') {
      this.angularFirestore.doc(`generalModule/students/${this.companyId}/${this.studentId}`).get().subscribe((studentDoc: any) => {
        if(studentDoc.exists) {
          let student = studentDoc.data()
          this.studentForm.patchValue(student)
        }
      })  
    }
  }

  createStudentForm() {
    this.studentForm = this.fb.group({
      name: ['', [ Validators.required ]],
      age: ['', [ Validators.required ]],
      gender: ['', [ Validators.required ]],
      phoneNumber: ['', [ Validators.compose([Validators.required, Validators.minLength(10)]) ]],
      email: ['', [ Validators.compose([Validators.required, Validators.email, this.emailValidation.bind(this)]) ]],
      department: ['', [ Validators.required ]],
      password: ['', [ Validators.compose([
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(10),
        Validators.pattern(
          "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{1,}$"
          )
      ])]]
      // confirmPassword: ['', [ Validators.compose([
      //   Validators.required,
      //   this.confirmPasswordValidation.bind(this)
      // ])]]
    })
  }

  get name() { return this.studentForm.get('name') as FormControl }
  get age() { return this.studentForm.get('age') as FormControl }
  get gender() { return this.studentForm.get('gender') as FormControl }
  get phoneNumber() { return this.studentForm.get('phoneNumber') as FormControl }
  get email() { return this.studentForm.get('email') as FormControl }
  get password() { return this.studentForm.get('password') as FormControl }
  // get confirmPassword() { return this.studentForm.get('confirmPassword') as FormControl }
  get department() { return this.studentForm.get('department') as FormControl }

  emailValidation(control: any) {
    if(control.value) {
      return control.value.endsWith(this.companyData.mail) ?  null:  { emailDomain: true };
    }
    return null;
  }

  // confirmPasswordValidation(control: FormControl) {
  //   if(control.value) {
  //     return control.value == this.studentForm.value.password ? null : { passwordMismatch: true };
  //   }
  //   return null;
  // }

  numericOnly(event): boolean {
    const charcode = event.which ? event.which : event.keyCode;
    if (charcode >= 48 && charcode <= 57) {
      return true;
    }
    return false;
  }

  phoneNumberValidation(event): boolean {
    const charcode = event.which ? event.which : event.keyCode;
    if ((charcode >= 48 && charcode <= 57) && (event.target.value.length < 10)) {
      return true;
    }
    return false;
  }
  
  alphabetsOnly(event): boolean {
    const charcode = event.which ? event.which : event.keyCode;
    if ((charcode >= 65 && charcode <= 90) || (charcode >= 97 && charcode <= 122) || charcode == 32) {
      return true;
    }
    return false;
  }

  updateStudentDetails() {
    if(this.studentId == 'null') {
      this.angularFirestore.collection(`generalModule/students/${this.companyId}`).add({ ...this.studentForm.value, createdAt: new Date() }).then((docRef: any) => {
        this.angularFirestore.doc(`generalModule/students/${this.companyId}/${docRef.id}`).update({ id: docRef.id });
      })
    } else {
      this.angularFirestore.doc(`generalModule/students/${this.companyId}/${this.studentId}`).update(this.studentForm.value)
    }
    this.router.navigate([`company/${this.companyId}`, { outlets: { right: 'studentlist'}}]);
  }

  goToStudentListPage() {
    this.router.navigate([`company/${this.companyId}`, { outlets: { right: 'studentlist'}}]);
  }

  onFileChange(event: any) {
    /* wire up file reader */
    const target: DataTransfer = <DataTransfer>(event.target);
    if (target.files.length !== 1) throw new Error('Cannot use multiple files');
    const reader: FileReader = new FileReader();
    reader.onload = (e: any) => {
      /* read workbook */
      const bstr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(bstr, { type: 'binary' });

      /* grab first sheet */
      const wsname: string = wb.SheetNames[0];
      const ws: XLSX.WorkSheet = wb.Sheets[wsname];

      /* save data */
      this.studentDataExcel = XLSX.utils.sheet_to_json(ws, { header: 1 });

      let studentData = {};
      this.studentDataExcel[0].forEach((key, index) => {
        studentData[`${key}`] = this.studentDataExcel[1][index];
      })
      this.studentForm.patchValue(studentData);
    };
    reader.readAsBinaryString(target.files[0]);
  }


  updateProfileExcel() {
    let element: HTMLElement = document.getElementById("updataProfileExcel") as HTMLElement;
    element.click();
  }

}
