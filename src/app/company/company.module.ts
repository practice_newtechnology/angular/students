import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CompanyRoutingModule } from './company-routing.module';
import { StudentFormComponent } from './student-form/student-form.component';
import { MaterialModule } from '../material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { CompanyHomeComponent } from './company-home/company-home.component';
import { StudentListComponent } from './student-list/student-list.component';
import { CourseFormComponent } from './course-form/course-form.component';
import { CourseListComponent } from './course-list/course-list.component';


@NgModule({
  declarations: [
    StudentFormComponent,
    CompanyHomeComponent,
    StudentListComponent,
    CourseFormComponent,
    CourseListComponent
  ],
  imports: [
    CommonModule,
    CompanyRoutingModule,
    MaterialModule,
    ReactiveFormsModule
  ]
})
export class CompanyModule { }
