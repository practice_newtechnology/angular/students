import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { ActivatedRoute, Router } from '@angular/router';
import { BehaviorSubject, combineLatest, Subject } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-course-list',
  templateUrl: './course-list.component.html',
  styleUrls: ['./course-list.component.scss']
})
export class CourseListComponent implements OnInit {
  companyId;
  companyId$ = new Subject<any>();
  searchCourses$ = new BehaviorSubject<string>('');
  courseList;
  ratings = [
    { isSelected: true },
    { isSelected: true },
    { isSelected: true },
    { isSelected: false },
    { isSelected: false }
  ];

  getAllCourses$ = this.companyId$.pipe(
    switchMap(companyId => {
      return this.angularFirestore.collection(`generalModule/courses/${companyId}`).valueChanges();
    })
  );

  getAllCoursesList$ = combineLatest([this.getAllCourses$, this.searchCourses$]).pipe(
    map((data: any) => {
      data[0] = data[0].filter(x => x.name.substring(0, data[1]?.length).trim().toLowerCase() == data[1]);
      return data[0];
    })
  ).subscribe(courselist => {
    this.courseList = courselist;
  });

  constructor(
    private angularFirestore: AngularFirestore,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.companyId = this.route.snapshot.parent.params['companyId'];
    this.companyId$.next(this.companyId);
  }

  searchCourses(searchString) {
    this.searchCourses$.next(searchString.trim().toLowerCase());
  }

  editCourseDetails(course) {
    this.router.navigate([`company/${this.companyId}`, { outlets: { right: `course/${course.id}`}}]);
  }

  deleteCourseDetails(course) {
    this.angularFirestore
      .doc(`generalModule/courses/${this.companyId}/${course.id}`)
      .delete();
  }

  ngOnDestroy() {
    this.getAllCoursesList$.unsubscribe();
  }

}
