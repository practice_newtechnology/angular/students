import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-course-form',
  templateUrl: './course-form.component.html',
  styleUrls: ['./course-form.component.scss']
})
export class CourseFormComponent implements OnInit {
  courseForm: FormGroup;
  companyId;
  courseId = null;
  companyData;

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private angularFirestore: AngularFirestore
  ) { }

  ngOnInit(): void {
    this.companyId = this.route.snapshot.parent.params['companyId'];
    this.courseId = this.route.snapshot.params['courseId'];
    this.createCourseForm();
    this.patchCourseDetails();
  }

  createCourseForm() {
    this.courseForm = this.fb.group({
      name: ['', [ Validators.required ]],
      description: ['', [ Validators.required ]],
      author: ['', [ Validators.required ]],
      price: ['', [ Validators.required ]],
    });
  }

  patchCourseDetails() {
    if(this.companyId != 'null') {
      this.angularFirestore.doc(`generalModule/courses/${this.companyId}/${this.courseId}`).get().subscribe((courseDoc: any) => {
        if(courseDoc.exists) {
          let course = courseDoc.data();
          this.courseForm.patchValue(course);
        }
      })  
    }
  }

  get name() { return this.courseForm.get('name') as FormControl }
  get description() { return this.courseForm.get('description') as FormControl }
  get author() { return this.courseForm.get('author') as FormControl }
  get price() { return this.courseForm.get('price') as FormControl }

  numericOnly(event): boolean {
    const charcode = event.which ? event.which : event.keyCode;
    return charcode >= 48 && charcode <= 57 ? true : false;
  }

  updateCourseDetails() {
    if(this.courseId == 'null') {
      this.angularFirestore.collection(`generalModule/courses/${this.companyId}`).add({ ...this.courseForm.value, createdAt: new Date() }).then((docRef: any) => {
        this.angularFirestore.doc(`generalModule/courses/${this.companyId}/${docRef.id}`).update({ id: docRef.id });
      })
    } else {
      this.angularFirestore.doc(`generalModule/courses/${this.companyId}/${this.courseId}`).update(this.courseForm.value)
    }
    this.router.navigate([`company/${this.companyId}`, { outlets: { right: 'courselist'}}]);
  }

  goToCourseListPage() {
    this.router.navigate([`company/${this.companyId}`, { outlets: { right: 'courselist'}}]);
  }

}
