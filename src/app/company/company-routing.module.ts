import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CompanyHomeComponent } from './company-home/company-home.component';
import { CourseFormComponent } from './course-form/course-form.component';
import { CourseListComponent } from './course-list/course-list.component';
import { StudentFormComponent } from './student-form/student-form.component';
import { StudentListComponent } from './student-list/student-list.component';

const routes: Routes = [
  {
    path: ':companyId',
    component: CompanyHomeComponent,
    children: [
      {
        path: 'studentlist',
        component: StudentListComponent,
        outlet: 'right'
      },
      {
        path: 'courselist',
        component: CourseListComponent,
        outlet: 'right'
      },
      {
        path: 'student/:studentId',
        component: StudentFormComponent,
        outlet: 'right'
      },
      {
        path: 'course/:courseId',
        component: CourseFormComponent,
        outlet: 'right'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CompanyRoutingModule { }
