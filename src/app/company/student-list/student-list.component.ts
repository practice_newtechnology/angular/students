import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { ActivatedRoute, Router } from '@angular/router';
import { map } from 'rxjs/operators';
import * as XLSX from 'xlsx';

@Component({
  selector: 'app-student-list',
  templateUrl: './student-list.component.html',
  styleUrls: ['./student-list.component.scss'],
})
export class StudentListComponent implements OnInit {
  companyId;
  studentData;
  fileName = 'StudentList.xlsx';

  constructor(
    private angularFirestore: AngularFirestore,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.companyId = this.route.snapshot.parent.params['companyId'];
    this.getStudentList();
  }

  getStudentList() {
    this.angularFirestore
    .collection(
      `generalModule/students/${this.companyId}`
    )
    .valueChanges()
    .pipe(
      map((studentList: any) => {
        studentList.map((student: any) => {
          student['createdAt'] = new Date(student.createdAt.seconds * 1000);
        });
        return studentList;
      })
    )
    .subscribe((studentList: any) => {
      this.studentData = studentList;
    });
  }

  deleteStudent(student) {
    this.angularFirestore
      .doc(`generalModule/students/${this.companyId}/${student.id}`)
      .delete();
  }

  goToStudentProfilePage(student) {
    this.router.navigate([`company/${this.companyId}`, { outlets: { right: `student/${student.id}`} }]);
  }

  downloadStudentList(): void {
    let data = [];
    data[0] = Object.keys(this.studentData[0]);
    this.studentData.forEach((student, i) => {
      let arr = new Array(8);
      for (const [key, value] of Object.entries(student)) {
        let index = data[0].findIndex((x) => x == key);
        arr[index] = value;
      }
      data[i + 1] = arr;
    });

    const ws: XLSX.WorkSheet = XLSX.utils.aoa_to_sheet(data);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
    XLSX.writeFile(wb, this.fileName);
  }

}
